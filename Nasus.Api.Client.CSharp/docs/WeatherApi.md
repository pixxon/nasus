# Nasus.Api.Client.CSharp.Api.WeatherApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**GetWeather**](WeatherApi.md#getweather) | **GET** /api/weather | Example weather query |

<a name="getweather"></a>
# **GetWeather**
> List&lt;WeatherForecast&gt; GetWeather ()

Example weather query

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Nasus.Api.Client.CSharp.Api;
using Nasus.Api.Client.CSharp.Client;
using Nasus.Api.Client.CSharp.Model;

namespace Example
{
    public class GetWeatherExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            var apiInstance = new WeatherApi(config);

            try
            {
                // Example weather query
                List<WeatherForecast> result = apiInstance.GetWeather();
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling WeatherApi.GetWeather: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetWeatherWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Example weather query
    ApiResponse<List<WeatherForecast>> response = apiInstance.GetWeatherWithHttpInfo();
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling WeatherApi.GetWeatherWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

[**List&lt;WeatherForecast&gt;**](WeatherForecast.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successfully returned |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

