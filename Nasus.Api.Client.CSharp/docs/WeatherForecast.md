# Nasus.Api.Client.CSharp.Model.WeatherForecast

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | 
**TemperatureC** | **int** |  | 
**TemperatureF** | **int** |  | 
**Summary** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

