import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { GroupListComponent } from './components/group-list/group-list.component';
import { GroupItemComponent } from './components/group-item/group-item.component';
import { EditorComponent } from './components/editor/editor.component';
import { WeatherComponent } from './components/weather/weather.component';
import { ApiModule, Configuration, ConfigurationParameters } from '@nasus/api-client';

const routes: Routes = [
  { path: '', component: GroupListComponent },
  { path: 'editor', component: EditorComponent },
  { path: 'weather', component: WeatherComponent }
]

export function apiConfigFactory (): Configuration {
  const params: ConfigurationParameters = {
    // set configuration parameters here.
    basePath: 'http://localhost:5000'
  }
  return new Configuration(params);
}

@NgModule({
  declarations: [
    AppComponent,
    GroupListComponent,
    GroupItemComponent,
    EditorComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    RouterModule.forRoot(routes),
    ApiModule.forRoot(apiConfigFactory),
    HttpClientModule,
    FormsModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
