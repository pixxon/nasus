import { Component } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ActivatedRoute } from '@angular/router';
import { Stage } from 'src/app/models/Stage';
import { GroupStage } from 'src/app/models/GroupStage';
import { Group } from 'src/app/models/Group';

/**
 * @title Drag&Drop sorting
 */
@Component({
  selector: 'editor.component',
  templateUrl: 'editor.component.html',
  styleUrls: ['editor.component.css'],
})
export class EditorComponent {
  groupStage : GroupStage = new GroupStage(
    0,
    'Group Stage',
    [
      { id: 0, name: 'Group A', teams: [
        { id: 0, name: 'T1', shortName: 'T1' },
        { id: 1, name: 'SGB', shortName: 'SGB' },
        { id: 2, name: 'DFM', shortName: 'DFM' },
        { id: 3, name: 'AZE', shortName: 'AZE' }
      ] },
      { id: 1, name: 'Group B', teams: [
        { id: 4, name: 'RNG', shortName: 'RNG' },
        { id: 5, name: 'PSG', shortName: 'PSG' },
        { id: 6, name: 'RED', shortName: 'RED' },
        { id: 7, name: 'IW', shortName: 'IW' }
      ] },
      { id: 2, name: 'Group C', teams: [
        { id: 8, name: 'G2', shortName: 'G2' },
        { id: 9, name: 'EG', shortName: 'EG' },
        { id: 10, name: 'ORD', shortName: 'ORD' }
      ] }
    ]
  )

  stages : Stage[] = [
    this.groupStage
  ];

  userId: string | null = null;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.userId = params['userId'];
    });
  }

  drop(event: CdkDragDrop<Group>) {
    moveItemInArray(event.container.data.teams, event.previousIndex, event.currentIndex);
//    moveItemInArray(this.movies, event.previousIndex, event.currentIndex);
  }

  isGroupStage(stage: Stage) : boolean {
    return (stage instanceof GroupStage)
  }

  asGroupStage(stage: Stage) : GroupStage {
    return (stage as GroupStage)
  }
}
