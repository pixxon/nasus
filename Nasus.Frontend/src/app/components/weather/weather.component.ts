import { Component } from '@angular/core';
import { WeatherService, WeatherForecast } from '@nasus/api-client';
import { Observable } from 'rxjs';

@Component({
  selector: 'weather.component',
  templateUrl: 'weather.component.html',
  styleUrls: ['weather.component.css']
})
export class WeatherComponent {
  constructor(service: WeatherService) {
	service.getWeather().subscribe((x) => {
		this.forecasts = x
	})
  }

  ngOnInit() {
  }

  forecasts: WeatherForecast[] = []
}
