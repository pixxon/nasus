import { Stage } from './Stage'
import { Group } from './Group'

export class GroupStage extends Stage {
	groups: Group[]

	constructor(id: number, name: string, groups: Group[]) {
		super(id, name);
		this.groups = groups;
	}
}
