# NasusApiClientJavascript.WeatherForecast

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **String** |  | 
**temperatureC** | **Number** |  | 
**temperatureF** | **Number** |  | 
**summary** | **String** |  | [optional] 


