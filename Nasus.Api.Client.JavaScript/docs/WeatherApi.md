# NasusApiClientJavascript.WeatherApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getWeather**](WeatherApi.md#getWeather) | **GET** /api/weather | Example weather query



## getWeather

> [WeatherForecast] getWeather()

Example weather query

### Example

```javascript
import NasusApiClientJavascript from '@nasus/api-client-javascript';

let apiInstance = new NasusApiClientJavascript.WeatherApi();
apiInstance.getWeather((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[WeatherForecast]**](WeatherForecast.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

