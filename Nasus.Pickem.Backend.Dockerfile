FROM mcr.microsoft.com/dotnet/aspnet:6.0-focal AS base
WORKDIR /app

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-dotnet-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

FROM mcr.microsoft.com/dotnet/sdk:6.0-focal AS build
WORKDIR /src
COPY ["Nasus.Shared/Nasus.Shared.csproj", "./Nasus.Shared/"]
COPY ["Nasus.Pickem.Backend.Data/Nasus.Pickem.Backend.Data.csproj", "./Nasus.Pickem.Backend.Data/"]
COPY ["Nasus.Pickem.Backend.SqliteMigrations/Nasus.Pickem.Backend.SqliteMigrations.csproj", "./Nasus.Pickem.Backend.SqliteMigrations/"]
COPY ["Nasus.Pickem.Backend.SqlServerMigrations/Nasus.Pickem.Backend.SqlServerMigrations.csproj", "./Nasus.Pickem.Backend.SqlServerMigrations/"]
COPY ["Nasus.Pickem.Backend/Nasus.Pickem.Backend.csproj", "./Nasus.Pickem.Backend/"]
RUN dotnet restore "Nasus.Pickem.Backend/Nasus.Pickem.Backend.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "Nasus.Pickem.Backend/Nasus.Pickem.Backend.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Nasus.Pickem.Backend/Nasus.Pickem.Backend.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Nasus.Pickem.Backend.dll"]
