using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System;

namespace Nasus.Backend.Data;

public class Stage
{
	[Key]
	public int ID { get; set; }
	public string Name { get; set; }
	[JsonIgnore]
	public Tournament Tournament { get; set; }
	public DateTime LockTime { get; set; }
}
