using System.Collections.Generic;

namespace Nasus.Backend.Data;

public class GroupStage
	: Stage
{
	public List<Group> Groups { get; set; }
}
