using System.Collections.Generic;

namespace Nasus.Backend.Data;

public class Round
{
	public int ID { get; set; }
	public string Name { get; set; }
	public List<Match> Matches { get; set; }
}
