using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Nasus.Backend.Data;

public class Tournament
{
	[Key]
	public int ID { get; set; }
	public string Name { get; set; }
	public List<Stage> Stages { get; set; }
}
