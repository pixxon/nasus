using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Nasus.Backend.Data;

public class Group
{
	[Key]
	public int ID { get; set; }

	public string Name { get; set; }

	public List<Team> Teams { get; set; }
}
