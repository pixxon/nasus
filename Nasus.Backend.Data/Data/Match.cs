using System.ComponentModel.DataAnnotations;

namespace Nasus.Backend.Data;

public class Match
{
	[Key]
	public int ID { get; set; }
	public Team? TeamA { get; set; }
	public Team? TeamB { get; set; }
}
