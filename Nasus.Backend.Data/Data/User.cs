using System.ComponentModel.DataAnnotations;

namespace Nasus.Backend.Data;

public class User
{
	[Key]
	public string ID { get; set; }
	public string Username { get; set; }
}
