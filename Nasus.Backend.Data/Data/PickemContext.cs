using Microsoft.EntityFrameworkCore;
using System;

namespace Nasus.Backend.Data;

public class PickemContext
	: DbContext
{
	public DbSet<Tournament> Tournaments => Set<Tournament>();
	public DbSet<Team> Teams => Set<Team>();
	public DbSet<User> Users => Set<User>();
	public DbSet<GroupGuess> Guesses => Set<GroupGuess>();
	public DbSet<TeamGuess> TeamGuesses => Set<TeamGuess>();

	public PickemContext(DbContextOptions<PickemContext> options)
		: base(options)
	{
	}
	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<Match>()
			.HasOne(e => e.TeamA)
			.WithMany()
			.OnDelete(DeleteBehavior.NoAction);
		modelBuilder.Entity<Match>()
			.HasOne(e => e.TeamB)
			.WithMany()
			.OnDelete(DeleteBehavior.NoAction);
		
//		modelBuilder.Entity<GroupStage>()
//			.ToTable("GroupStage");
		
//		modelBuilder.Entity<KnockoutStage>()
//			.ToTable("KnockoutStage");
		
		var team_RNG = new { ID = 1, Name = "Royal Never Give Up", ShortName = "RNG", GroupsId = 2 };
		var team_T1 = new { ID = 2, Name = "T1", ShortName = "T1", GroupsId = 1 };
		var team_G2 = new { ID = 3, Name = "G2 Esports", ShortName = "G2", GroupsId = 3 };
		var team_EG = new { ID = 4, Name = "Evil Geniuses", ShortName = "EG", GroupsId = 3};
		var team_PSG = new { ID = 5, Name = "PSG Talon", ShortName = "PSG", GroupsId = 2 };
		var team_SGB = new { ID = 6, Name = "Saigon Buffalo", ShortName = "SGB", GroupsId = 1 };
		var team_IW = new { ID = 7, Name = "Istanbul Wildcats", ShortName = "IW", GroupsId = 2 };
		var team_AZE = new { ID = 8, Name = "Team Aze", ShortName = "AZE", GroupsId = 1 };
		var team_RED = new { ID = 9, Name = "RED Canids", ShortName = "RED", GroupsId = 2 };
		var team_DFM = new { ID = 10, Name = "DetonatioN FM", ShortName = "DFM", GroupsId = 1 };
		var team_ORD = new { ID = 11, Name = "ORDER", ShortName = "ORD", GroupsId = 3 };

		var groupA = new { ID = 1, Name = "Group A", GroupStageID = 1 };
		var groupB = new { ID = 2, Name = "Group B", GroupStageID = 1 };
		var groupC = new { ID = 3, Name = "Group C", GroupStageID = 1 };

		var groupStage = new { ID = 1, Name = "Stage 1 (Groups)", LockTime = new DateTime(2022, 05, 10, 7, 0, 0, DateTimeKind.Utc) , TournamentID = 1, TournamentID1 = 1 };

		var msi2022 = new { ID = 1, Name = "MSI 2022" };

		modelBuilder.Entity<Team>().HasData(team_RNG);
		modelBuilder.Entity<Team>().HasData(team_T1);
		modelBuilder.Entity<Team>().HasData(team_G2);
		modelBuilder.Entity<Team>().HasData(team_EG);
		modelBuilder.Entity<Team>().HasData(team_PSG);
		modelBuilder.Entity<Team>().HasData(team_SGB);
		modelBuilder.Entity<Team>().HasData(team_IW);
		modelBuilder.Entity<Team>().HasData(team_AZE);
		modelBuilder.Entity<Team>().HasData(team_RED);
		modelBuilder.Entity<Team>().HasData(team_DFM);
		modelBuilder.Entity<Team>().HasData(team_ORD);

		modelBuilder.Entity<Group>().HasData(groupA);
		modelBuilder.Entity<Group>().HasData(groupB);
		modelBuilder.Entity<Group>().HasData(groupC);

		modelBuilder.Entity<GroupStage>().HasData(groupStage);

		modelBuilder.Entity<Tournament>().HasData(msi2022);

		modelBuilder.Entity<Team>()
			.HasMany(t => t.Groups)
			.WithMany(g => g.Teams)
			.UsingEntity(e => 
			{
				e.HasData( new { TeamsID = team_T1.ID, GroupsID = groupA.ID } );
				e.HasData( new { TeamsID = team_SGB.ID, GroupsID = groupA.ID } );
				e.HasData( new { TeamsID = team_DFM.ID, GroupsID = groupA.ID } );
				e.HasData( new { TeamsID = team_AZE.ID, GroupsID = groupA.ID } );
				e.HasData( new { TeamsID = team_IW.ID, GroupsID = groupB.ID } );
				e.HasData( new { TeamsID = team_PSG.ID, GroupsID = groupB.ID } );
				e.HasData( new { TeamsID = team_RED.ID, GroupsID = groupB.ID } );
				e.HasData( new { TeamsID = team_RNG.ID, GroupsID = groupB.ID } );
				e.HasData( new { TeamsID = team_EG.ID, GroupsID = groupC.ID } );
				e.HasData( new { TeamsID = team_G2.ID, GroupsID = groupC.ID } );
				e.HasData( new { TeamsID = team_ORD.ID, GroupsID = groupC.ID } );
			}
		);
	}
}
