using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace Nasus.Backend.Data;

public class TeamGuess
{
	[Key]
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int ID { get; set; }
	public int Order { get; set; }
	public Team Team { get; set; }

	[JsonIgnore]
	public GroupGuess GroupGuess { get; set; }
}
public class GroupGuess
{
	[Key]
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int ID { get; set; }
	public User User { get; set; }
	public Group Group { get; set; }
	public List<TeamGuess> Guesses { get; set; }
}
