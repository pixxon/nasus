using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace Nasus.Backend.Data;

public class Team
{
	[Key]
	public int ID { get; set; }
	public string Name { get; set; }
	public string ShortName { get; set; }
	[JsonIgnore]
	public List<Group> Groups { get; set; }
}
