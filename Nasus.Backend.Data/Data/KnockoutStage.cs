using System.Collections.Generic;

namespace Nasus.Backend.Data;

public class KnockoutStage
	: Stage
{
	public List<Round> Rounds { get; set; }
}
