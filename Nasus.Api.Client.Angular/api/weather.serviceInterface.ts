/**
 * Weather API
 * A simple API to illustrate OpenAPI concepts
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders }                                       from '@angular/common/http';

import { Observable }                                        from 'rxjs';

import { WeatherForecast } from '../model/models';


import { Configuration }                                     from '../configuration';



export interface WeatherServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * Example weather query
     * 
     */
    getWeather(extraHttpRequestParams?: any): Observable<Array<WeatherForecast>>;

}
