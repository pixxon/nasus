FROM node:lts-alpine as build
WORKDIR /usr/src/app

COPY Nasus.Pickem.Frontend/ .
RUN npm install --silent
RUN npm run build

FROM nginx:latest
COPY --from=build /usr/src/app/dist/NasusPickemFrontend /usr/share/nginx/html
EXPOSE 80
