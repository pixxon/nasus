﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Discord;
// using Discord.Commands;
using Discord.Addons.Hosting;

var host = Host.CreateDefaultBuilder()
	.ConfigureHostConfiguration(builder =>
	{
		builder.AddEnvironmentVariables(conf =>
		{
			conf.Prefix = "ASPNETCORE_";
		});
	})
	.ConfigureAppConfiguration((context, app) =>
	{
		app.AddJsonFile(o =>
		{
			o.Path = "appsettings.json";
			o.Optional = true;
			o.ReloadOnChange = true;
		});
		app.AddJsonFile(o =>
		{
			o.Path = $"appsettings.{context.HostingEnvironment.EnvironmentName}.json";
			o.Optional = true;
			o.ReloadOnChange = true;
		});
		app.AddEnvironmentVariables(o =>
		{
			o.Prefix = "NASUS_";
		});
	})
	.ConfigureLogging((context, log) =>
	{
		log.AddConfiguration(context.Configuration.GetSection("Logging"));
		log.AddConsole();
	})
	.ConfigureDiscordHost((context, disc) =>
	{
		disc.Token = context.Configuration.GetValue<String>("Token");
	})
//	.UseCommandService((context, config) =>
//	{
//		config.DefaultRunMode = RunMode.Async;
//		config.CaseSensitiveCommands = false;
//	})
	.UseInteractionService((context, config) =>
	{
		config.LogLevel = LogSeverity.Info;
		config.UseCompiledLambda = true;
	})
	.ConfigureServices(services =>
	{
		services.AddHostedService<NasusBot.Services.BotStatusService>();
//		services.AddHostedService<NasusBot.Services.CommandHandler>();
		services.AddHostedService<NasusBot.Services.InteractionHandler>();
	}).Build();

await host.RunAsync();
