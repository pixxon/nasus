using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Net.Http.Json;
using System.Web;

using Discord;
using Discord.WebSocket;
using Discord.Interactions;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace NasusBot.Modules;

[Group("pickem", "all the commands for your pickem")]
public class PickemModule
	: InteractionModuleBase<SocketInteractionContext>
{
	private readonly HttpClient _httpClient;
	private readonly RSA _privateKey;

	public PickemModule(IHostEnvironment environment, IConfiguration configuration)
	{
		_privateKey = RSA.Create();
		_privateKey.ImportRSAPrivateKey(Convert.FromBase64String(configuration.GetValue<String>("PrivateKey")), out _);

		if (environment.IsDevelopment())
		{
			var handler = new HttpClientHandler
			{
				ClientCertificateOptions = ClientCertificateOption.Manual,
				ServerCertificateCustomValidationCallback = (_, _, _, _) => true
			};
			_httpClient = new HttpClient(handler);
		}
		else
		{
			_httpClient = new HttpClient();
		}
		_httpClient.BaseAddress = new Uri(configuration.GetValue<String>("WebUrl"));
	}

	[SlashCommand("help", "list commands")]
	public async Task Help()
	{
		await DeferAsync();

		var commands = new List<String>
		{
			"`help` - prints this list",
			"`join` - joins the current servers list",
			"`edit` - provides link to edit picks"
		};

		var embed = new EmbedBuilder()
			.WithTitle("Pickem")
			.WithCurrentTimestamp()
			.WithDescription(String.Join("\n", commands))
			.WithColor(Color.Green)
			.Build();
		await FollowupAsync(embed: embed);
	}

	[SlashCommand("join", "join pickem")]
	public async Task Join()
	{
		try
		{
			await DeferAsync();
			var payload = new Nasus.Shared.Requests.CreateRequest
			{
				UserId = $"{Context.User.Id}",
				Username = $"{Context.User.Username}#{Context.User.Discriminator}"
			};

			var signature = System.Convert.ToBase64String(_privateKey.SignData(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(payload)), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1));

			var request = new HttpRequestMessage
			{
				Method = HttpMethod.Post,
				Content = JsonContent.Create(payload),
				RequestUri = new System.Uri("api/user/register", UriKind.Relative)
			};
			request.Headers.Add("NasusSignature", signature);

			var result = await _httpClient.SendAsync(request);

			var embedBuilder = new EmbedBuilder()
				.WithAuthor(Context.User.ToString(), Context.User.GetAvatarUrl() ?? Context.User.GetDefaultAvatarUrl())
				.WithTitle("Pickem")
				.WithCurrentTimestamp();

			switch(result.StatusCode)
			{
				case System.Net.HttpStatusCode.OK:
				{
					embedBuilder.WithDescription("Joined successfully")
						.WithColor(Color.Green);
					break;
				}
				case System.Net.HttpStatusCode.Forbidden:
				{
					embedBuilder.WithDescription("Something went wrong")
						.WithColor(Color.Red);
					break;
				}
				case System.Net.HttpStatusCode.Conflict:
				{
					embedBuilder.WithDescription("Already joined!")
						.WithColor(Color.Orange);
					break;
				}
			}
			
			await FollowupAsync(embed: embedBuilder.Build(), ephemeral: true);
		}
		catch
		{

			var embed = new EmbedBuilder()
				.WithTitle("Pickem")
				.WithCurrentTimestamp()
				.WithDescription("Something went really wrong")
				.WithColor(Color.Red)
				.Build();
			await FollowupAsync(embed: embed);
		}
	}

	public enum Tournament
	{
		[ChoiceDisplay("MSI 2022")]
		MSI2022 = 1
	}

	[SlashCommand("edit", "edit pickem guess")]
	public async Task Edit(Tournament tournament)
	{
		await DeferAsync(ephemeral: true);

		try
		{
			var payload = new Nasus.Shared.Requests.EditorRequest
			{
				UserId = $"{Context.User.Id}",
				TournamentId = 1,
				Timestamp = $"{DateTime.UtcNow.AddMinutes(30).Ticks}"
			};
			var signature = System.Convert.ToBase64String(_privateKey.SignData(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(payload)), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1));
			
			var embed = new EmbedBuilder()
				.WithAuthor(Context.User.ToString(), Context.User.GetAvatarUrl() ?? Context.User.GetDefaultAvatarUrl())
				.WithTitle("Pickem")
				.WithCurrentTimestamp()
				.WithDescription($"Your link is {_httpClient.BaseAddress}Editor?userId={System.Uri.EscapeDataString(payload.UserId)}&tournamentId={payload.TournamentId}&timestamp={payload.Timestamp}&nasusSignature={System.Uri.EscapeDataString(signature)}")
				.WithColor(Color.Green)
				.Build();
			await FollowupAsync(embed: embed, ephemeral: true);
		}
		catch
		{
			var embed = new EmbedBuilder()
				.WithTitle("Pickem")
				.WithCurrentTimestamp()
				.WithDescription("Something went really wrong")
				.WithColor(Color.Red)
				.Build();
			await FollowupAsync(embed: embed, ephemeral: true);
		}
	}
}
