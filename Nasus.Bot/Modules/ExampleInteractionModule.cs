using Discord.Interactions;

namespace NasusBot.Modules;

public class ExampleInteractionModule
	: InteractionModuleBase<SocketInteractionContext>
{
	[SlashCommand("echo", "Echo an input")]
	public async Task Echo(string input)
	{
		await RespondAsync(input);
	}
}
