using System.Reflection;

using Discord;
using Discord.Addons.Hosting;
using Discord.Addons.Hosting.Util;
using Discord.Interactions;
using Discord.WebSocket;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;

namespace NasusBot.Services;

internal class InteractionHandler
	: DiscordClientService
{
	private readonly IServiceProvider _provider;
	private readonly InteractionService _interactionService;
	private readonly IHostEnvironment _environment;

	public InteractionHandler(DiscordSocketClient client, ILogger<DiscordClientService> logger, IServiceProvider provider, InteractionService interactionService, IHostEnvironment environment)
		: base(client, logger)
	{
		_provider = provider;
		_interactionService = interactionService;
		_environment = environment;
	}

	protected override async Task ExecuteAsync(CancellationToken stoppingToken)
	{
		Client.InteractionCreated += HandleInteraction;
		await _interactionService.AddModulesAsync(Assembly.GetEntryAssembly(), _provider);
		await Client.WaitForReadyAsync(stoppingToken);

		if (_environment.IsDevelopment())
		{
			await _interactionService.RegisterCommandsToGuildAsync(692518914109997120);
		}
		else
		{
			await _interactionService.RegisterCommandsGloballyAsync();
		}
	}

	private async Task HandleInteraction(SocketInteraction arg)
	{
		try
		{
			var ctx = new SocketInteractionContext(Client, arg);
			await _interactionService.ExecuteCommandAsync(ctx, _provider);
		}
		catch (Exception ex)
		{
			Logger.LogError(ex, "Exception occurred whilst attempting to handle interaction.");
			
			if (arg.Type == InteractionType.ApplicationCommand)
			{
				var msg = await arg.GetOriginalResponseAsync();
				await msg.DeleteAsync();
			}
		}
	}
}
