using Microsoft.AspNetCore.Mvc;

using Nasus.Api.Server.CSharp.Controllers;
using Nasus.Api.Server.CSharp.Models;

namespace Nasus.Backend.Controllers;

public class WeatherForecastController
    : WeatherApiController
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    public override IActionResult GetWeather()
    {
        var forecast = Enumerable.Range(1, 5)
            .Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index).ToString(),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            });
        return Ok(forecast);
    }
}
