using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Nasus.Backend.Controllers;

[IgnoreAntiforgeryToken]
[Route("api/[controller]/[action]")]
public class PickemController
	: ControllerBase
{
	private readonly ILogger<PickemController> _logger;
	private readonly Nasus.Shared.Services.ISignatureService _signatureVerifier;
	private readonly Nasus.Backend.Data.PickemContext _db;

	public PickemController(ILogger<PickemController> logger, Nasus.Shared.Services.ISignatureService signatureVerifier, Nasus.Backend.Data.PickemContext db)
	{
		_logger = logger;
		_signatureVerifier = signatureVerifier;
		_db = db;
	}

	public async Task<IActionResult> Get(string userId, int tournamentId /* , string timestamp, string nasusSignature */)
	{
		var payload = new Nasus.Shared.Requests.EditorRequest
		{
			UserId = userId,
			TournamentId = tournamentId,
			Timestamp = "" // timestamp
		};
/*
		if(!_signatureVerifier.VerifySignature(payload, nasusSignature))
		{
			return StatusCode(403);
		}

		if(new DateTime(Int64.Parse(timestamp)) < DateTime.UtcNow)
		{
			return StatusCode(403);
		}
*/
		var Tournament = _db.Tournaments
			.Where(t => t.ID == payload.TournamentId)
			.Include(t => t.Stages)
			.ThenInclude(s => (s as Data.GroupStage).Groups)
			.ThenInclude(g => g.Teams)
			.FirstOrDefault();
		if(Tournament == null)
		{
			return StatusCode(404);
		}

		var PickemUser = _db.Users.Where(u => u.ID == payload.UserId).FirstOrDefault();
		if(PickemUser == null)
		{
			PickemUser = _db.Users.Add(new Data.User{ ID = payload.UserId, Username = "ezubot" }).Entity;
			await _db.SaveChangesAsync();
//			return StatusCode(404);
		}

		var Guesses = new List<Nasus.Backend.Data.GroupGuess>();
		foreach(var s in Tournament.Stages)
		{
			var groupStage = s as Data.GroupStage;
			if(groupStage != null)
			{
				foreach(var group in groupStage.Groups)
				{
					var guess = _db.Guesses
						.Where(g => g.User.ID == PickemUser.ID && g.Group.ID == group.ID)
						.Include(x => x.Guesses)
						.FirstOrDefault();
					
					if(guess == null)
					{
						var defaultGuess = new List<Data.TeamGuess>();
						foreach(var (t, i) in group.Teams.OrderBy(_ => Guid.NewGuid()).Select((t, i) => (t, i)))
						{
							defaultGuess.Add(new Data.TeamGuess{ Order = i, Team = t });
						}
						guess = _db.Guesses.Add(new Data.GroupGuess{ User = PickemUser, Group = group, Guesses = defaultGuess }).Entity;
					}

					Guesses.Add(guess);
				}
				await _db.SaveChangesAsync();
			}
			else
			{
				return StatusCode(403);
			}
		}

		foreach(var s in Tournament.Stages)
		{
			var groupStage = s as Data.GroupStage;
			if(groupStage != null)
			{
				foreach(var group in groupStage.Groups)
				{
					var x = Guesses.Where(g => g.Group.ID == group.ID).FirstOrDefault();
					Func<Data.Team, int> l = t => x.Guesses.Where(e => e.Team.ID == t.ID).FirstOrDefault().Order;
					group.Teams.Sort((a, b) => l.Invoke(a) - l.Invoke(b));
				}
			}
			else
			{
				return StatusCode(403);
			}
		}

		var FakePayload = payload;
		var FakeSignature = ""; // nasusSignature;

		return Ok(new {
			Tournament = Tournament,
			PickemUser = PickemUser,
			Guesses = Guesses,
			FakePayload = FakePayload,
			FakeSignature = FakeSignature,
		});
	}
}
