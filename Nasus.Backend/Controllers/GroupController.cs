using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Nasus.Backend.Controllers;

[IgnoreAntiforgeryToken]
[Route("api/[controller]/[action]")]
public class GroupController
	: ControllerBase
{
	private readonly ILogger<GroupController> _logger;
	private readonly Nasus.Shared.Services.ISignatureService _signatureVerifier;
	private readonly Nasus.Backend.Data.PickemContext _db;

	public GroupController(ILogger<GroupController> logger, Nasus.Shared.Services.ISignatureService signatureVerifier, Nasus.Backend.Data.PickemContext db)
	{
		_logger = logger;
		_signatureVerifier = signatureVerifier;
		_db = db;
	}

	[HttpPost]
	public IActionResult Guess([FromBody] Nasus.Shared.Requests.GroupGuessRequest payload, [FromHeader] string nasusSignature)
	{
		if(!_signatureVerifier.VerifySignature(payload.FakePayload, System.Uri.UnescapeDataString(nasusSignature)))
		{
			return StatusCode(403);
		}

		var user = _db.Users.Where(u => u.ID == payload.UserId).FirstOrDefault();
		if(user == null)
		{
			return StatusCode(404);
		}

		var tournament = _db.Tournaments
			.Where(t => t.ID == payload.TournamentId)
			.Include(t => t.Stages)
			.ThenInclude(s => (s as Data.GroupStage).Groups)
			.ThenInclude(g => g.Teams)
			.FirstOrDefault();
		if(tournament == null)
		{
			return StatusCode(404);
		}

		var stage = tournament.Stages.Where(s => s.ID == payload.StageId).FirstOrDefault() as Data.GroupStage;
		if(stage == null)
		{
			return StatusCode(404);
		}
		if(stage.LockTime <= DateTime.UtcNow)
		{
			return StatusCode(403);
		}
		if(new DateTime(Int64.Parse(payload.FakePayload.Timestamp)) < DateTime.UtcNow)
		{
			return StatusCode(403);
		}

		var group = stage.Groups.Where(g => g.ID == payload.GroupId).FirstOrDefault();
		if(group == null)
		{
			return StatusCode(404);
		}

		var guess = _db.Guesses
			.Where(g => g.User.ID == user.ID && g.Group.ID == group.ID)
			.Include(g => g.Group)
			.Include(g => g.Guesses)
			.ThenInclude(g => g.Team)
			.FirstOrDefault();
		if(guess == null)
		{
			return StatusCode(404);
		}

		foreach(var g in payload.Guess)
		{
			var team = group.Teams.Where(t => t.ID == g.TeamId).FirstOrDefault();
			if(team == null)
			{
				return StatusCode(404);
			}
			if(g.Position < 0 || g.Position >= group.Teams.Count)
			{
				return StatusCode(404);
			}
			guess.Guesses
				.Where(t => t.Team.ID == team.ID)
				.ToList()
				.ForEach(e => e.Order = g.Position);
		}
		_db.SaveChanges();

		return StatusCode(200);
	}
}
