using Microsoft.AspNetCore.Mvc;

namespace Nasus.Backend.Controllers;

[IgnoreAntiforgeryToken]
[Route("api/[controller]/[action]")]
public class UserController
	: ControllerBase
{
	private readonly ILogger<UserController> _logger;
	private readonly Nasus.Shared.Services.ISignatureService _signatureVerifier;
	private readonly Nasus.Backend.Data.PickemContext _db;

	public UserController(ILogger<UserController> logger, Nasus.Shared.Services.ISignatureService signatureVerifier, Nasus.Backend.Data.PickemContext db)
	{
		_logger = logger;
		_signatureVerifier = signatureVerifier;
		_db = db;
	}

	[HttpPost]
	public async Task<IActionResult> Register([FromBody] Nasus.Shared.Requests.CreateRequest payload, [FromHeader] string nasusSignature)
	{
		if(!_signatureVerifier.VerifySignature(payload, nasusSignature))
		{
			return StatusCode(403);
		}

		var user = _db.Users
			.Where(u => u.ID == payload.UserId)
			.FirstOrDefault();
		if(user != null)
		{
			return StatusCode(409);
		}

		try
		{
			_db.Users.Add(new Data.User{ ID = payload.UserId, Username = payload.Username });
			await _db.SaveChangesAsync();
			return StatusCode(200);
		}
		catch
		{
			return StatusCode(500);
		}
	}

	[HttpGet]
	public IActionResult Find([FromQuery] string userId)
	{
		var user = _db.Users
			.Where(u => u.ID == userId)
			.FirstOrDefault();
		if(user == null)
		{
			return NotFound();
		}

		return Ok(new Nasus.Shared.Responses.FindResponse
		{
			UserId = user.ID,
			Username = user.Username
		});
	}
}
