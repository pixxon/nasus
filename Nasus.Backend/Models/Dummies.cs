namespace Nasus.Backend.Models;

using Nasus.Backend.Data;

public class Dummies
{
	public static Team Dummy_Team_RNG = new Team
	{
		ID = 0,
		Name = "RNG",
		ShortName = "RNG"
	};
	public static Team Dummy_Team_G2 = new Team
	{
		ID = 1,
		Name = "G2 Esports",
		ShortName = "G2"
	};
	public static Team Dummy_Team_T1 = new Team
	{
		ID = 2,
		Name = "T1",
		ShortName = "T1"
	};
	public static Team Dummy_Team_TL = new Team
	{
		ID = 3,
		Name = "Team Liquid",
		ShortName = "TL"
	};
	public static Team Dummy_Team_PSG = new Team
	{
		ID = 4,
		Name = "PSG Talon",
		ShortName = "PSG"
	};
	public static Team Dummy_Team_GAM = new Team
	{
		ID = 5,
		Name = "GAM Esports",
		ShortName = "GAM"
	};
	public static Team Dummy_Team_AZE = new Team
	{
		ID = 6,
		Name = "Team Aze",
		ShortName = "AZE"
	};
	public static Team Dummy_Team_IW = new Team
	{
		ID = 7,
		Name = "Istanbul Wildcats",
		ShortName = "IW"
	};
	public static Team Dummy_Team_PNG = new Team
	{
		ID = 8,
		Name = "paiN Gaming",
		ShortName = "PNG"
	};
	public static Team Dummy_Team_DFM = new Team
	{
		ID = 9,
		Name = "DetonatioN FM",
		ShortName = "DFM"
	};
	public static Team Dummy_Team_ORD = new Team
	{
		ID = 10,
		Name = "ORDER",
		ShortName = "ORD"
	};

	public static Group Dummy_Group_A = new Group
	{
		ID = 0,
		Name = "Group A",
		Teams = new List<Team>
		{
			Dummy_Team_RNG,
			Dummy_Team_G2,
			Dummy_Team_T1
		}
	};
	public static Group Dummy_Group_B = new Group
	{
		ID = 1,
		Name = "Group B",
		Teams = new List<Team>
		{
			Dummy_Team_TL,
			Dummy_Team_PSG,
			Dummy_Team_GAM,
			Dummy_Team_AZE
		}
	};
	public static Group Dummy_Group_C = new Group
	{
		ID = 2,
		Name = "Group B",
		Teams = new List<Team>
		{
			Dummy_Team_IW,
			Dummy_Team_PNG,
			Dummy_Team_DFM,
			Dummy_Team_ORD
		}
	};

	public static GroupStage Dummy_GroupStage_Groups = new GroupStage
	{
		ID = 0,
		Name = "Group stage",
		Groups = new List<Group>
		{
			Dummy_Group_A,
			Dummy_Group_B,
			Dummy_Group_C
		}
	};

	public static GroupStage Dummy_GroupStage_Rumble = new GroupStage
	{
		ID = 1,
		Name = "Rumble stage",
		Groups = new List<Group>
		{
			new Group
			{
				ID = 4,
				Name = "Stage 2",
				Teams = new List<Team>
				{
					Dummy_Team_T1,
					Dummy_Team_G2,
					Dummy_Team_RNG,
					Dummy_Team_TL,
					Dummy_Team_PSG,
					Dummy_Team_GAM
				}
			}
		}
	};

	public static Team Dummy_LPL_Team_BLG = new Team
	{
		ID = 11,
		Name = "Bilibili Gaming",
		ShortName = "BLG"
	};
	public static Team Dummy_LPL_Team_EDG = new Team
	{
		ID = 12,
		Name = "EDward Gaming",
		ShortName = "EDG"
	};
	public static Team Dummy_LPL_Team_FPX = new Team
	{
		ID = 13,
		Name = "FuncPlus Phoenix",
		ShortName = "FPX"
	};
	public static Team Dummy_LPL_Team_JDG = new Team
	{
		ID = 14,
		Name = "JD Gaming",
		ShortName = "JDG"
	};
	public static Team Dummy_LPL_Team_LNG = new Team
	{
		ID = 15,
		Name = "LNG Esports",
		ShortName = "LNG"
	};
	public static Team Dummy_LPL_Team_RA = new Team
	{
		ID = 16,
		Name = "Rare Atom",
		ShortName = "RA"
	};
	public static Team Dummy_LPL_Team_RNG = new Team
	{
		ID = 17,
		Name = "RNG",
		ShortName = "RNG"
	};
	public static Team Dummy_LPL_Team_TES = new Team
	{
		ID = 18,
		Name = "Top Esports",
		ShortName = "TES"
	};
	public static Team Dummy_LPL_Team_V5 = new Team
	{
		ID = 19,
		Name = "Victory Five",
		ShortName = "V5"
	};
	public static Team Dummy_LPL_Team_WBG = new Team
	{
		ID = 20,
		Name = "Weibo Gaming",
		ShortName = "WBG"
	};

	public static Round Dummy_LPL_Round_1 = new Round
	{
		ID = 1,
		Name = "Round 1",
		Matches = new List<Match>
		{
			new Match
			{
				TeamA = Dummy_LPL_Team_BLG,
				TeamB = Dummy_LPL_Team_RA
			},
			new Match
			{
				TeamA = Dummy_LPL_Team_EDG,
				TeamB = Dummy_LPL_Team_FPX
			}
		}
	};

	public static Round Dummy_LPL_Round_2 = new Round
	{
		ID = 2,
		Name = "Round 2",
		Matches = new List<Match>
		{
			new Match
			{
				TeamA = Dummy_LPL_Team_TES
			},
			new Match
			{
				TeamA = Dummy_LPL_Team_WBG
			}
		}
	};

	public static Round Dummy_LPL_Round_3 = new Round
	{
		ID = 3,
		Name = "Round 3",
		Matches = new List<Match>
		{
			new Match
			{
				TeamA = Dummy_LPL_Team_LNG
			},
			new Match
			{
				TeamA = Dummy_LPL_Team_JDG
			}
		}
	};

	public static Round Dummy_LPL_Round_4 = new Round
	{
		ID = 4,
		Name = "Round 4",
		Matches = new List<Match>
		{
			new Match
			{
				TeamA = Dummy_LPL_Team_V5
			},
			new Match
			{
				TeamA = Dummy_LPL_Team_RNG
			},
			new Match
			{
			}
		}
	};

	public static Stage Dummy_LPL_Playoffs = new KnockoutStage
	{
		ID = 2,
		Name = "Spring Playoffs",
		Rounds = new List<Round>
		{
			Dummy_LPL_Round_1,
			Dummy_LPL_Round_2,
			Dummy_LPL_Round_3,
			Dummy_LPL_Round_4
		}
	};
};
