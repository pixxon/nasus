using Microsoft.EntityFrameworkCore;
using Nasus.Api.Server.CSharp.Formatters;
using Nasus.Api.Server.CSharp.Converters;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables(o => o.Prefix = "NASUS_");

builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));

builder.Services.AddControllers(options => {
    options.InputFormatters.Insert(0, new InputFormatterStream());
}).AddNewtonsoftJson(opts => {
    opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
    opts.SerializerSettings.Converters.Add(new StringEnumConverter {
        NamingStrategy = new CamelCaseNamingStrategy()
    });
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<Nasus.Shared.Services.ISignatureService, Nasus.Shared.Services.NasusSignatureService>();

var provider = builder.Configuration.GetValue("Provider", "SqlServer");
builder.Services.AddDbContext<Nasus.Backend.Data.PickemContext>(
    options => _ = provider switch
    {
        "Sqlite" => options.UseSqlite(
            builder.Configuration.GetConnectionString("SqliteConnection"),
            x => x.MigrationsAssembly("Nasus.Backend.Migrations.Sqlite")
        ),
        "SqlServer" => options.UseSqlServer(
            builder.Configuration.GetConnectionString("SqlServerConnection"),
            x => x.MigrationsAssembly("Nasus.Backend.Migrations.SqlServer")
        ),
        _ => throw new Exception($"Unsupported provider: {provider}")
    }
);

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var db = scope.ServiceProvider.GetRequiredService<Nasus.Backend.Data.PickemContext>();
    db.Database.Migrate();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("corsapp");

app.MapControllers();

app.Run();
