import React, { useState } from 'react';

function Weather (props) {
	var [ data, setData ] = useState([]);
	if (data.length === 0) {
		props.client.getWeather((error, data, response) => setData(data));
	}
	return (
		<div>
			{ data.map((e) =>(
				<div key={e.date}>{e.date}</div>
			))}
		</div>
		
	)
}
export default Weather;
