import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Weather from './component/weather';
import { WeatherApi, ApiClient } from '@nasus/api-client-javascript';

function App() {
	var asd = new ApiClient('http://localhost:5000');
	var client = new WeatherApi(asd);
	return (
		<BrowserRouter>
			<Routes>
				<Route exact path='/weather' element={< Weather client={client} /> }></Route>
			</Routes>
		</BrowserRouter>
	);
}

export default App;
