namespace Nasus.Shared.Services;

public interface ISignatureService
{
	bool VerifySignature<T>(T payload, string signature);
	string CreateSignature<T>(T payload);
}
