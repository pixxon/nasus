using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

using Microsoft.Extensions.Configuration;

namespace Nasus.Shared.Services;

public class NasusSignatureService
	: ISignatureService
{
	private readonly RSA _key;

	public NasusSignatureService(IConfiguration configuration)
	{
		_key = RSA.Create();
		_key.ImportRSAPrivateKey(Convert.FromBase64String(configuration.GetValue<String>("PrivateKey")), out _);
	}

	public bool VerifySignature<T>(T payload, string signature)
	{
		return _key.VerifyData(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(payload)), System.Convert.FromBase64String(signature), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
	}
	public string CreateSignature<T>(T payload)
	{
		return System.Convert.ToBase64String(_key.SignData(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(payload)), HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1));
	}
}
