namespace Nasus.Shared.Responses;

public class FindResponse
{
	public string UserId { get; set; }
	public string Username { get; set; }
}
