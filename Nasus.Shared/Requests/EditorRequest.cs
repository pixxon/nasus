namespace Nasus.Shared.Requests;

public class EditorRequest
{
	public string UserId { get; set; }
	public int TournamentId { get; set; }
	public string Timestamp { get; set; }
}
