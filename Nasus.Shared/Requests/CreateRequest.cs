namespace Nasus.Shared.Requests;

public class CreateRequest
{
	public string UserId { get; set; }
	public string Username { get; set; }
}
