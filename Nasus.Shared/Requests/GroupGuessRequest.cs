namespace Nasus.Shared.Requests;

public class GroupGuessRequest
{
	public class GroupGuess
	{
		public int TeamId { get; set; }
		public int Position { get; set; }
	}

	public string UserId { get; set; }
	public int TournamentId { get; set; }
	public int GroupId { get; set; }
	public int StageId { get; set; }
	public List<GroupGuess> Guess { get; set; }
	public EditorRequest FakePayload { get; set; }
}
