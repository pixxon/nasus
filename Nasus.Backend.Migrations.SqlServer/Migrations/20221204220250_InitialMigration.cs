﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Nasus.Backend.Migrations.SqlServer.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ShortName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Tournaments",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tournaments", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Match",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamAID = table.Column<int>(type: "int", nullable: true),
                    TeamBID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Match", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Match_Teams_TeamAID",
                        column: x => x.TeamAID,
                        principalTable: "Teams",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_Match_Teams_TeamBID",
                        column: x => x.TeamBID,
                        principalTable: "Teams",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "Stage",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TournamentID = table.Column<int>(type: "int", nullable: false),
                    LockTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Discriminator = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stage", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Stage_Tournaments_TournamentID",
                        column: x => x.TournamentID,
                        principalTable: "Tournaments",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Group",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GroupStageID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Group_Stage_GroupStageID",
                        column: x => x.GroupStageID,
                        principalTable: "Stage",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "GroupTeam",
                columns: table => new
                {
                    GroupsID = table.Column<int>(type: "int", nullable: false),
                    TeamsID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupTeam", x => new { x.GroupsID, x.TeamsID });
                    table.ForeignKey(
                        name: "FK_GroupTeam_Group_GroupsID",
                        column: x => x.GroupsID,
                        principalTable: "Group",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupTeam_Teams_TeamsID",
                        column: x => x.TeamsID,
                        principalTable: "Teams",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Guesses",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    GroupID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guesses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Guesses_Group_GroupID",
                        column: x => x.GroupID,
                        principalTable: "Group",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Guesses_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "TeamGuesses",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    TeamID = table.Column<int>(type: "int", nullable: false),
                    GroupGuessID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeamGuesses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TeamGuesses_Guesses_GroupGuessID",
                        column: x => x.GroupGuessID,
                        principalTable: "Guesses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeamGuesses_Teams_TeamID",
                        column: x => x.TeamID,
                        principalTable: "Teams",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "ID", "Name", "ShortName" },
                values: new object[,]
                {
                    { 1, "Royal Never Give Up", "RNG" },
                    { 2, "T1", "T1" },
                    { 3, "G2 Esports", "G2" },
                    { 4, "Evil Geniuses", "EG" },
                    { 5, "PSG Talon", "PSG" },
                    { 6, "Saigon Buffalo", "SGB" },
                    { 7, "Istanbul Wildcats", "IW" },
                    { 8, "Team Aze", "AZE" },
                    { 9, "RED Canids", "RED" },
                    { 10, "DetonatioN FM", "DFM" },
                    { 11, "ORDER", "ORD" }
                });

            migrationBuilder.InsertData(
                table: "Tournaments",
                columns: new[] { "ID", "Name" },
                values: new object[] { 1, "MSI 2022" });

            migrationBuilder.InsertData(
                table: "Stage",
                columns: new[] { "ID", "Discriminator", "LockTime", "Name", "TournamentID" },
                values: new object[] { 1, "GroupStage", new DateTime(2022, 5, 10, 7, 0, 0, 0, DateTimeKind.Utc), "Stage 1 (Groups)", 1 });

            migrationBuilder.InsertData(
                table: "Group",
                columns: new[] { "ID", "GroupStageID", "Name" },
                values: new object[] { 1, 1, "Group A" });

            migrationBuilder.InsertData(
                table: "Group",
                columns: new[] { "ID", "GroupStageID", "Name" },
                values: new object[] { 2, 1, "Group B" });

            migrationBuilder.InsertData(
                table: "Group",
                columns: new[] { "ID", "GroupStageID", "Name" },
                values: new object[] { 3, 1, "Group C" });

            migrationBuilder.InsertData(
                table: "GroupTeam",
                columns: new[] { "GroupsID", "TeamsID" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 1, 6 },
                    { 1, 8 },
                    { 1, 10 },
                    { 2, 1 },
                    { 2, 5 },
                    { 2, 7 },
                    { 2, 9 },
                    { 3, 3 },
                    { 3, 4 },
                    { 3, 11 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Group_GroupStageID",
                table: "Group",
                column: "GroupStageID");

            migrationBuilder.CreateIndex(
                name: "IX_GroupTeam_TeamsID",
                table: "GroupTeam",
                column: "TeamsID");

            migrationBuilder.CreateIndex(
                name: "IX_Guesses_GroupID",
                table: "Guesses",
                column: "GroupID");

            migrationBuilder.CreateIndex(
                name: "IX_Guesses_UserID",
                table: "Guesses",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Match_TeamAID",
                table: "Match",
                column: "TeamAID");

            migrationBuilder.CreateIndex(
                name: "IX_Match_TeamBID",
                table: "Match",
                column: "TeamBID");

            migrationBuilder.CreateIndex(
                name: "IX_Stage_TournamentID",
                table: "Stage",
                column: "TournamentID");

            migrationBuilder.CreateIndex(
                name: "IX_TeamGuesses_GroupGuessID",
                table: "TeamGuesses",
                column: "GroupGuessID");

            migrationBuilder.CreateIndex(
                name: "IX_TeamGuesses_TeamID",
                table: "TeamGuesses",
                column: "TeamID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupTeam");

            migrationBuilder.DropTable(
                name: "Match");

            migrationBuilder.DropTable(
                name: "TeamGuesses");

            migrationBuilder.DropTable(
                name: "Guesses");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "Group");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Stage");

            migrationBuilder.DropTable(
                name: "Tournaments");
        }
    }
}
